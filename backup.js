//post admin
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "945d48c6-e14c-3ad7-fb7c-628b1f65342f"
    },
    "processData": false,
    "data": "{\n   \"admin\" :\n   {\n       \"username\" :\"bhagvat\",\n       \"password\":\"john123\",\n       \"firstName\" : \"peter\",\n       \"lastName\" : \"sen\",\n       \"email\": \"landebm@gmail.com\",\n       \"mobile\": \"+917972729505\"\n   \n   }\n}"
};

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("admin added suceessfully" + "<br>" + "<hr>");
});

//get single admin
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/5893103c1cbfff000160cd50",
    "method": "GET",
    "headers": {
        "cache-control": "no-cache",
        "postman-token": "cff723e3-ccd0-2115-fd07-94a825ae3b4f"
    }
}

$.ajax(settings).done(function(response) {
    console.log(response);
    data = response.result.user;
    //   var user = response.result.users[0].address[0];
    var data = alertobjectKeys(data);
    $(".console").append("<hr>");
});

//function to display object
function alertobjectKeys(data) {
    for (var key in data) {
        if (typeof(data[key]) == "object" && data[key] != null) {
            alertobjectKeys(data[key]);
        } else {
            $('#console').append(key + " : " + data[key] + "<br>");

        }
    }
}

//admin activate
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/5893103c1cbfff000160cd50/activate",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "9214c931-2dd0-31d9-3872-6bdec64a3ac8"
    },
    "processData": false,
    "data": "{\n\t\"otp\":\"535210\"\n}"
};
$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("admin activated successfully" + response.result + "<br>" + "<hr>");
});

//admin login
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/login",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "apikey": "12345",
        "cache-control": "no-cache",
        "postman-token": "8b2bbede-630c-065f-30d8-f7f9a73e5aad"
    },
    "processData": false,
    "data": "{\n\t\"admin\":\n\t{\n\t\t\"email\":\"landebm@gmail.com\",\n\t\t\"password\":\"john12345\"\n\t}\n}"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("admin logged in  suceessfully:" + response.result.token + "<br>" + "<hr>");
});


//admin logout
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/589315b31cbfff000160cd57/logout",
    "method": "POST",
    "headers": {
        "cache-control": "no-cache",
        "postman-token": "04b276da-da1f-c405-0bac-4eb74506f31a"
    }
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append(response.result + "<br>" + "<hr>");
});

//forget password (send otp over mail)
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/5893103c1cbfff000160cd50/forgotpassword/email",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "6b34805c-9764-8822-c288-75315cdebdec"
    },
    "processData": false,
    "data": "{\n\t\"email\":\"landenm@gmail.com\"\n}"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("otp sent on Email.." + response.result + "<br>" + "<hr>");
});

//forget password (send otp over mobile)
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/5893103c1cbfff000160cd50/forgotpassword/sms",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "3b0e6c41-d790-4258-d471-97dd2f4d0b2f"
    },
    "processData": false,
    "data": "{\n\t\"mobile\":\"+917972729505\"\n}"
};

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("otp sent on mobile.." + response.result + "<br>" + "<hr>");
});

//change password
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/5893103c1cbfff000160cd50/Changepassword",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "429e8d7b-521c-5fd3-596c-bd180ad658f7"
    },
    "processData": false,
    "data": "{\n\t\"otp\":\"535210\",\n\t\"password\":\"john12345\",\n\t\"confPassword\":\"john12345\"\n}"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("password changes successfully" + response.result + "<br>" + "<hr>");
});

//update admin
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/admin/589315b31cbfff000160cd57",
    "method": "PUT",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "640e5acd-6fef-c80c-a063-cedaeaef3344"
    },
    "processData": false,
    "data": "{\n   \"admin\" :\n   {\n       \"username\" :\"rajni updated\",\n       \"password\":\"john123\",\n       \"adminType\":\"admin\",\n       \"firstName\" : \"peter\",\n       \"lastName\" : \"sen\",\n       \"email\": \"rajaniwankhede8@gmail.com\",\n       \"mobile\": \"+917972729555\"\n   \n   }\n} "
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("admin updated successfully:" + response.result.ok + "<br>" + "<hr>");
});

//update super admin
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/updatesuperadmin/5893103c1cbfff000160cd50",
    "method": "PUT",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "bedd5001-fc3b-1b20-6460-47d9c530a860"
    },
    "processData": false,
    "data": "{\n\t\"admin\":\n\t{\n    \"username\" : \"bhagvat updated\", \n    \"password\" : \"$2a$10$GDv97d4FDfF0Yj2oGv0.euxXUVrLJH2nFinphCqP7j.K1aVak3WDS\", \n    \"firstName\" : \"peter\", \n    \"adminType\" : \"superAdmin\", \n    \"lastName\" : \"sen\", \n    \"email\" : \"landebm@gmail.com\", \n    \"mobile\" : \"+917972729505\", \n    \"status\" : \"active\"\n}\n}"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("superAdmin updated successfully:" + response.result.ok + "<br>" + "<hr>");
});

//delete admin (only superAdmin can delete)
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/deleteadmin/5893103c1cbfff000160cd50/589315b31cbfff000160cd57",
    "method": "DELETE",
    "headers": {
        "cache-control": "no-cache",
        "postman-token": "7aac9f23-1989-d665-af94-44a4ff828c73"
    }
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("admin deleted successfully:" + response.result.ok + "<br>" + "<hr>");
});