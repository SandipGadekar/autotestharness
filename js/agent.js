//post plan
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/plan",
    "method": "POST",
    "headers": {
        "content-type": "application/json",
        "apikey": "12345",
        "authorization": "eyJhbGciOiJIUzI1NiJ9.bGFuZGVibUBnbWFpbC5jb21qb2huMTIzNDU.YyGUg3toaZzpupD_JDBcAEE2YGp3XBQb9euxeIGdr7g",
        "cache-control": "no-cache",
        "postman-token": "16bf1a45-7ba8-22b9-b59b-ba1ac611eed7"
    },
    "processData": false,
    "data": "{\n    \"plan\" :\n    {\n        \"name\":\"lifetime plan\",\n        \"price\":\"2000$\",\n        \"frequency\":\"5\",\n        \"status\":\"inactive\",\n        \"credits\":\"5\",\n        \"currency\":\"usd\",\n        \"unit\":\"year\",\n        \"trialperiod\":\"5\",\n        \"trialperiodUnit\":\"week\",\n        \"trialperiodFrequency\":\"5\"\n        }\n}\n\n"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("plan added suceessfully:" + response.result.ok + "<br>" + "<hr>");
});

//get plan(single) by all
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/plan/589330cf1cbfff000160cd5d",
    "method": "GET",
    "headers": {
        "apikey": "12345",
        "cache-control": "no-cache",
        "postman-token": "25fe149b-ffbe-ea46-e537-57f09b054f48"
    }
}

$.ajax(settings).done(function(response) {
    console.log(response);
    data = response.result.plan;
    //   var user = response.result.users[0].address[0];
    var data = alertobjectKeys(data);
    $(".console").append("<hr>");
});

//function to display object
function alertobjectKeys(data) {
    for (var key in data) {
        if (typeof(data[key]) == "object" && data[key] != null) {
            alertobjectKeys(data[key]);
        } else {
            $('#console').append(key + " : " + data[key] + "<br>");

        }
    }
}


//update plan
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/plan/589330cf1cbfff000160cd5d",
    "method": "PUT",
    "headers": {
        "content-type": "application/json",
        "authorization": "eyJhbGciOiJIUzI1NiJ9.bGFuZGVibUBnbWFpbC5jb21qb2huMTIzNDU.YyGUg3toaZzpupD_JDBcAEE2YGp3XBQb9euxeIGdr7g",
        "apikey": "12345",
        "cache-control": "no-cache",
        "postman-token": "70b23efa-f987-fd56-3498-f509199a3bcd"
    },
    "processData": false,
    "data": "{\n\t\"plan\":\n\t{\n    \"name\" : \"lifetime plan updTAED\", \n    \"price\" : \"2000$\", \n    \"frequency\" : \"5\", \n    \"status\" : \"inactive\", \n    \"credits\" : \"5\", \n    \"currency\" : \"usd\", \n    \"unit\" : \"year\", \n    \"trialperiod\" : \"5\", \n    \"trialperiodUnit\" : \"week\", \n    \"trialperiodFrequency\" : \"5\"\n\t}\n}"
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("plan updated suceessfully:" + response.result.ok + "<br>" + "<hr>");
});

//delete plan
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.17.0.3:8000/userapi/plan/589337091cbfff000160cd60",
    "method": "DELETE",
    "headers": {
        "content-type": "application/json",
        "authorization": "eyJhbGciOiJIUzI1NiJ9.bGFuZGVibUBnbWFpbC5jb21qb2huMTIzNDU.YyGUg3toaZzpupD_JDBcAEE2YGp3XBQb9euxeIGdr7g",
        "apikey": "12345",
        "cache-control": "no-cache",
        "postman-token": "0adf59e9-cca3-be24-26d4-f08bd41ef70b"
    },
    "processData": false,
    "data": ""
}

$.ajax(settings).done(function(response) {
    console.log(response);
    $(".console").append("plan deleted suceessfully:" + response.result.ok + "<br>" + "<hr>");
});