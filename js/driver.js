$(document).ready(function() {
    $("#post").click(function() {
        //Add Driver
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://autosearchapp.herokuapp.com/driver",
            "method": "POST",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "064371d0-e402-bbdd-cf2f-f0dfd2a79122"
            },
            "processData": false,
            "data": "{\n\t\"driver\":{\n\t\t\"firstName\": \"rajani\",\n\t\t\"lastName\": \"wankhede\",\n\t\t\"age\":\"25\",\n\t\t\"gender\": \"femel\",\n\t\t\"mobile\":\"+919860626638\",\n\t\t\"batchno\":\"78965\",\n\t\t\"licenceNumber\":\"mh46 mm1234\",\n\t\t\"adharNumber\":\"1234567891\",\n\t\t\"licenceImage\":\"abc.png\",\n\t\t\"adharImage\":\"pqr.png\",\n\t\t\"driverImage\":\"stq.png\",\n\t\t\"password\":\"rajani\"\n\t\t\n\t\t\n\t\t\n\t}\n}"
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            $(".console").append("driver added suceessfully" + "<br>" + "<hr>");
        });
    });
    $("#Singleget").click(function() {
        //Get Driver
        var id = $('#id').val();
        if (id == '')
            alert("Please enter id ");
        else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://autosearchapp.herokuapp.com/getDriver/" + id,
                "method": "GET",
                "headers": {
                    "authorization": "eyJhbGciOiJIUzI1NiJ9.dW5kZWZpbmVkcmFqYW5p.AwzNw4OtR2IfPBIh6BfRux2bnVFvTTt8BR-w4YoXN68",
                    "cache-control": "no-cache",
                    "postman-token": "38d62984-af20-8f13-7086-d7344dc0a214"
                }
            }
            $.ajax(settings).done(function(response) {

                console.log(response);
                data = response.result.user;
                var data = alertobjectKeys(data);
                $(".console").append("<hr>");
            });
            //function to display object
            function alertobjectKeys(data) {
                for (var key in data) {
                    if (typeof(data[key]) == "object" && data[key] != null) {
                        alertobjectKeys(data[key]);
                    } else {
                        $('#console').append(key + " : " + data[key] + "<br>");

                    }
                }
            }
        }
    });
    $("#activate").click(function() {
        //Admin activate
        var id = $('#id').val();
        var otp = $('#otp').val();
        if (id == '' || otp == '')
            alert("Please enter id and otp fields");
        else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://172.17.0.17:8000/admin/admin/" + id + "/activate",
                "method": "POST",
                "headers": {
                    "key": "john123",
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "0386150d-9028-8070-8121-4b88019778dc"
                },
                "processData": false,
                "data": "{\"otp\" : \"" + otp + "\"}"
            }

            $.ajax(settings).done(function(response) {
                console.log(response);
                $(".console").append("admin activated suceessfully" + "<br>" + "<hr>");
            });
        }
    });
    $("#login").click(function() {
        //Admin login
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://172.17.0.17:8000/admin/admin/login",
            "method": "POST",
            "headers": {
                "key": "john123",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "8e504735-796f-8404-18e4-7a9fcae7f301"
            },
            "processData": false,
            "data": "{\n\t\"admin\":\n\t{\n\t\t\"email\":\"landebm@gmail.com\",\n\t\t\"password\":\"john123\"\n\t}\n}"
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            var token = response.result.token;
            $(".console").append("admin login suceessfully" + token + "<br>" + "<hr>");
        });
    });
    $("#logout").click(function() {
        //Admin logout
        var id = $('#id').val();
        if (id == '')
            alert("Please enter id ");
        else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://172.17.0.17:8000/admin/admin/" + id + "/logout",
                "method": "POST",
                "headers": {
                    "key": "john123",
                    "cache-control": "no-cache",
                    "postman-token": "99239836-d33e-8e38-7ce9-c4deb484513c"
                }
            }

            $.ajax(settings).done(function(response) {
                console.log(response);
                $(".console").append("admin logout suceessfully" + "<br>" + "<hr>");
            });
        }
    });
    //DRIVER forgot SMS
    $("#forgetsms").click(function() {
        var batchno = $('batchno').val();
        if (batchno == '')
            alert("Please enter batchno ");
        else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://autosearchapp.herokuapp.com/driver/forgotpassword/sms",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "a306e1e6-b8c7-0d9f-abd0-edf1d21466f0"
                },
                "processData": false,
                "data": "{\n\t\"batchno\":\"78965\"\n}"
            }


            $.ajax(settings).done(function(response) {
                console.log(response);
                $(".console").append("driver forgot password send sms" + "<br>" + "<hr>");
            });
        }
    });
    //change password
    $("#change").click(function() {
        var batchno = $('batchno').val();
        var otp = $('#otp').val();
        if (batchno == '' || otp == '')
            alert("Please enter batchno and otp fields");
        else {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://autosearchapp.herokuapp.com/driver/Changepassword",
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "0c0e5b01-0df6-1826-31a8-33c6102fef91"
                },
                "processData": false,
                "data": "{\n\t\n\t\t\"otp\":\"028864\",\n\t\t\"password\":\"111\",\n\t\t\"confPassword\":\"111\"\n}"
            }

            $.ajax(settings).done(function(response) {
                console.log(response);
                $(".console").append("Password change successfully" + "<br>" + "<hr>");
            });
        }
    });
    $("#put").click(function() {
        //updated 
        var id = $('#id').val();
        if (id == '')
            alert("Please enter id ");
        else {

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://autosearchapp.herokuapp.com/updateDriver/" + id,
                "method": "PUT",
                "headers": {
                    "content-type": "application/json",
                    "authorization": "eyJhbGciOiJIUzI1NiJ9.dW5kZWZpbmVkcmFqYW5p.AwzNw4OtR2IfPBIh6BfRux2bnVFvTTt8BR-w4YoXN68",
                    "cache-control": "no-cache",
                    "postman-token": "5d96576e-1dcc-9272-7aa5-36c54a546009"
                },
                "processData": false,
                "data": "{\n\t\"driver\":{\n\t\t\"firstName\": \"rajani upd\",\n\t\t\"lastName\": \"wankhede\",\n\t\t\"age\":\"21\",\n\t\t\"gender\": \"female\",\n\t\t\"mobile\":\".+918888804265\",\n\t\t\"batchno\":\"78965\",\n\t\t\"licenceNumber\":\"MH12pqr1b234\",\n\t\t\"adharNumber\":\"1111111111\",\n\t\t\"licenceImage\":\"abc.png\",\n\t\t\"adharImage\":\"pqr.png\",\n\t\t\"driverImage\":\"stq.png\",\n\t\t\"password\":\"1234\",\n\t\t\"confPassword\":\"1234\"\n\t\t\n\t\t\n\t\t\n\t}\n}\n\n"
            }

            $.ajax(settings).done(function(response) {
                $(".console").append("Updated successfully" + "<br>" + "<hr>");
            });
        }

    });
    $("#delete").click(function() {
        // //delete admin
        var id = $('#id').val();
        if (id == '')
            alert("Please enter id");
        else {

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://autosearchapp.herokuapp.com/deleteDriver/58de0877a38a020011ead638",
                "method": "DELETE",
                "headers": {
                    "authorization": "eyJhbGciOiJIUzI1NiJ9.dW5kZWZpbmVkcmFqYW5p.sQvMl_plRh5yphU0NVTUR5PeAZMNApIxwxCVeMU_UoU",
                    "cache-control": "no-cache",
                    "postman-token": "30fce81e-104a-24af-af7e-83c88a5eb0fb"
                }
            }


            $.ajax(settings).done(function(response) {
                console.log(response);
                $(".console").append("admin deleted successfully" + response.result + "<br>" + "<hr>");
            });
        }
    });
});